//
// Created by fabio on 17/03/20.
//

#include "Usuario.h"
#include <utility>
#include <map>
#include <iomanip>

using namespace std;

//Contructores
Usuario::Usuario(){
    this->ci = "";
    this->nombre = "";
    this->fechaIngreso = fechaIngreso;
    this->colViaje;
}

Usuario::Usuario(string ci, string nombre, DtFecha fechaIngreso){
    this->ci = ci;
    this->nombre = nombre;
    this->fechaIngreso = fechaIngreso;
    this->colViaje;
}

//Getters
string Usuario::getCi() {
    return this->ci;
}

string Usuario::getNombre() {
    return this->nombre;
}

DtFecha Usuario::getFechaIngreso() {
    return this->fechaIngreso;
}

//Setters

void Usuario::setCi(string ci) {
    this->ci = ci;
}

void Usuario::setNombre(string nombre) {
    this->nombre = nombre;
}

void Usuario::setFechaIngreso(DtFecha fechaIngreso) {
    this->fechaIngreso = fechaIngreso;
}


//Destructor
Usuario::~Usuario()=default;


DtFecha Usuario::ObtenerFechaSistema(){
    int d;
    int m;
    int an;
    time_t tSac = time(NULL); // instante actual
    struct tm* tmP = localtime(&tSac);
    d=tmP->tm_mday;
    m=tmP->tm_mon+1;
    an=tmP->tm_year+1900;

    DtFecha fecha = DtFecha(d, m , an);

    return fecha;
}

void Usuario::ingresarViaje(string ci, int nroSerieVehiculo, const DtViajeBase& viaje){
}

//void Usuario::ingresarViajeB (DtViaje viaje){
//
//}

DtViaje verViajesAntesDeFecha(const DtFecha& fecha, string ci, int& cantViajes){
    //return nullptr;
}

void eliminarViajes(string ci, const DtFecha& fecha){

}

void Usuario::getViajes(){
//	return NULL;
}
int Usuario::getCantVehiculos(){
	return 0;
}

map<string,DtViaje> Usuario::getViajesAntesFecha(DtFecha fechaIngresada){

    map<string,DtViaje> resultado;
    int duracion;
    int distancia;
    DtViaje viaje;
    DtFecha fechaViaje;


    //DtFecha fechaSistema = ObtenerFechaSistema();
    int numeroViaje=1;

    for(map<string,Viaje*>::iterator it=colViaje.begin(); it != colViaje.end(); ++it ){
        string clave = to_string(numeroViaje);
        duracion = it->second->getDuracion();
        distancia = it->second->getDistancia();
        fechaViaje = it->second->getFecha();

        //viaje = DtViaje(fechaViaje,duracion,distancia);

     /*   if (fechaIngresada < fechaViaje){
            resultado.insert(make_pair(clave,viaje));
        }*/
        numeroViaje++;
    }
    return resultado;
}

int Usuario::cantViajes(){
	return 0;
}
