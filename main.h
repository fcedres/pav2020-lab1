//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_MAIN_H
#define PAV2020_LAB1_MAIN_H

#include <iostream>
#include <cstring>
#include <sstream>
#include <typeinfo>
#include <utility>
#include <string>
#include <iomanip>
#include "Usuario.h"
#include "EnumTipoBici.h"
#include "DtViaje.h"
#include "DtFecha.h"
#include "DtViajeBase.h"
#include "DtMonopatin.h"
#include "DtBicicleta.h"
#include "DtVehiculo.h"
#include "Constante.h"
#include "Viaje.h"
#include <map>

#define clearscr() printf("\033[H\033[J");

using namespace std;

class Menu {

};


#endif //PAV2020_LAB1_MAIN_H
