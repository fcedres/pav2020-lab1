//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_DTVEHICULO_H
#define PAV2020_LAB1_DTVEHICULO_H
#include <string>

using std::string;


class DtVehiculo {
private:
    int nroSerie;
    float porcentajeBateria;
    float precioBase;

public:
    //Constructor
    DtVehiculo();
    DtVehiculo(int nroSerie, float porcentajeBateria, float precioBase);

    //Geters
    int getNroSerie();
    float getPorcentajeBateria();
    float getPrecioBase();

    //Destructor
    virtual ~DtVehiculo();
};
#endif //PAV2020_LAB1_DTVEHICULO_H
