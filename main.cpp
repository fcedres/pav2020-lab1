//
// Created by fabio on 17/03/20.
//
#include "main.h"

// Colección de Socios
map <string,Usuario*> colUsuarios;
map <int,Vehiculo*> colVehiculos;

bool existeUsuario(string ci){// esta funcion devuelve true si existe el
	 //usuario de cedula ci y false si no existe
    if ( colUsuarios.find(ci) != colUsuarios.end()){
        return  true;
    }else{
        return false;
    }
}

void ingresarUsuario(string ci, DtViaje viaje, DtFecha fechaIngreso,string nombre){
	try{
		Usuario *nuevoUsuario = new Usuario(ci,nombre,fechaIngreso); //crea un nuevo objeto del tipo usuario
//		}
//		catch (const std::exception& newUser){
//		}
	cout << nombre << " ha sido ingresado de forma correcta al sistema";
//	nuevoUsuario->ingresarViaje(viaje); // me parece que no es en esta parte que va
//	nuevoUsuario->ingresarViajeB(viaje); // me parece que no es en esta parte que va
//	nuevoUsuario->ingresarViaje(viaje);
	colUsuarios.insert(make_pair(ci,nuevoUsuario));
	}
	catch (const std::exception& addColection){
	}
}

void agregarVehiculo(const DtVehiculo& vehiculo){

}

void ingresarViaje(string ci, int nroSerieVehiculo, const DtViajeBase& viaje){

}

DtViaje verViajeAntesDeFecha(const DtFecha& fecha, string ci, int& cantViajes){
	DtViaje viaje;
	return viaje;
}

//void eliminarViajes(string ci, const DtFecha& fecha){
//
//}

void cambiarBateriaVehiculo(string ci, const DtFecha& fecha){

}

DtVehiculo obtenerVehiculos(int cantVehiculos){
	DtVehiculo vehiculo;
	return vehiculo;
}

void listarUsuarios(){



	cout << " " << left<< setw(12) << "Cedula" << left << setw(35)
		        << "Nombre" << endl;

	for (map<string, Usuario*>::iterator it = colUsuarios.begin();
			 	 	 	 	 	 	 	 it != colUsuarios.end(); ++it){

	        cout << " " << left<< setw(12) << it->first << left << setw(35)
	        << it->second->getNombre() << left << endl;
	    }
}


void menu() {
    clearscr();

    cout << endl << "    Bienvenido!" << endl << endl;

    cout << "   Menu principal" << endl;
    cout << "    1- Registrar Usuario" << endl;
    cout << "    2- Agregar Vehículo" << endl;
    cout << "    3- Ingresar Viaje" << endl;
    cout << "    4- Ver Viajes Antes de Fecha" << endl;
    cout << "    5- Eliminar Viajes" << endl;
    cout << "    6- Listar Usuarios" << endl;
    cout << "    7- Obtener Vehículos" << endl;
    cout << "    8- Cambiar Bateria Vehículo" << endl;
    cout << "    0- Salir" << endl;
    cout << endl << "  Elija una opcion: ";
}

string cedulaValida() {
    int cedula=0;
    cout << "Ingrese la cedula: ";
    cin >> cedula;
    //cin.clear();

    while (cedula == ' ' || cedula == 0) {
        cout << "Error CEDULA" << endl;
        cin.clear();
        cout << "Ingrese la cedula: ";
        cin >> cedula;
        cin.clear();
    }
    return to_string(cedula);
}

//bool existeSocio(string ci){
//
//    if ( colUsuarios.find(ci) != colUsuarios.end()){
//        return  true;
//    }else{
//        return false;
//    }
//} Ya la habia implementdo arriba, con el nombre de la función correcta. ahora es Usuario no Socio


bool existeNroSerie(int nroSerie){

    if ( colVehiculos.find(nroSerie) != colVehiculos.end()){
        return  true;
    }else{
        return false;
    }
}

void obtenerVehiculos(string ci) {

    cout << "Listado de Vehiculos del Usuario" << endl;
    cout << " " << left << setw(12) << "CI" << left << setw(35) << "NOMBRE" << "FECHA DE INGRESO \n" << endl;
    cout << " " << left << setw(12) << ci << left << setw(35) << colUsuarios.find(ci)->second->getNombre() << endl;
    //cout << "\n Vehículos:" << colUsuarios.find(ci)->second->getCantVehiculos() << endl;

    //map<string, DtVehiculo*> vehiculos = colUsuarios.find(ci)->second->getCantVehiculos();

    //for (map<string,  DtVehiculo*>::iterator it = vehiculos.begin(); it != vehiculos.end(); ++it) {


        //cout << it->second;
//                try {
//
//                    auto dtperro = dynamic_cast<DtPerro *>  (it->second);
//                    if(dtperro!= nullptr) {
//                        cout << " " << left << setw(12) << dtperro->getNombre() << left << setw(25)
//                             << dtperro->getGenero() << left << setw(30)
//                             << left << setw(35) << dtperro->getPeso() << left << setw(40) << dtperro->getRacionDiaria() << left << setw(45)
//                             << dtperro->getRaza() << left << setw(50)
//                             << dtperro->getVacunaCachorro() << endl;
//                    }
//                } catch (std::bad_cast &e) { cout << "error de casteo" << endl; }
//
//                    try {
//
//                        auto dtgato = dynamic_cast<DtGato *> (it->second);
//                        if(dtgato!= nullptr) {
//                            cout << " " << left << setw(12) << dtgato->getNombre() << left << setw(25)
//                                 << dtgato->getGenero() << left << setw(30) << dtgato->getPeso() << left << setw(35)
//                                 << dtgato->getRacionDiaria() << left << setw(40) << dtgato->getTipoPelo()
//                                 << endl;
//                        }
//                    } catch (std::bad_cast &e) { cout << "error de casteo" << endl; }

    //}     cout << "\n  " << colUsuarios.find(ci)->second->getCantVehiculos() << " Vehiculos " << endl;
}

void opcion_1() {
    //Registrar Usuario

    clearscr();

    cout << "Registrar Usuario\n" << endl;
    if ((colUsuarios.size()) > (MAX_USUARIOS - 1)){
    		cout << "Se ha alcanzado el máximo de usuarios permitidos en el sistema" << endl;
    	}
    else{

		DtViaje viaje;
		string ci,nombre;
		DtFecha fechaIngreso;
		int d,m,a;

		cout << "ingrese la cedula: ";
		cin >> ci;

		if (existeUsuario(ci)){
		cout << "El usuario ya existe en el sistema" << endl;
		}
		else{
			cout << "ingrese el nombre: ";
			cin >> nombre;
			cout << "ingrese el dia: ";
			cin >> d ;
			cout << "ingrese el mes: ";
			cin >> m;
			cout << "ingrese el anio: ";
			cin >> a;

			fechaIngreso = DtFecha(d,m,a);//  .setAnio(a);

			try{
			ingresarUsuario(ci,viaje,fechaIngreso,nombre);
			}catch(const std::exception& e){
			}
		}
    }
}

void opcion_2() {
    //Agregar Vehiculo

    clearscr();
    cout << "Agregar Vehículo\n" << endl;

    int opcion;
    int nroSerie;
    float porcentajeBateria,precioBase;
    DtMonopatin monopatin;
    DtBicicleta bicicleta;

    cout << "1- Agregar Monopatin" << endl;
    cout << "2- Agregar Bicicleta" << endl;
    cout << "ingrese una opcion: ";
    cin >> opcion;

    switch(opcion){

		case 1://opción Crear monopatin

			bool tieneLuces;
			int luces;

			cout << "Ingrese el nro de serie: ";
			cin >> nroSerie;
			cout << "ingrese el porcentaje de bateria: ";
			cin >> porcentajeBateria;
			cout << "ingrese el precio base: ";
			cin >> precioBase;
			cout << "¿Tiene luces S/N?" << endl;
			cout << "	0-si" << endl;
			cout << "	1-no ";
			cin >> luces;

			if (luces==0){
				tieneLuces = true;
			}
			if (luces==1){
				tieneLuces = false;
			}
			monopatin = DtMonopatin (nroSerie, porcentajeBateria, precioBase, tieneLuces);
			break;

		case 2:
			// opcion crear Bicicleta

			int cantCambios,op_tipobici;
			tipo tipoBici;

			cout << "Ingrese el nro de serie: ";
			cin >> nroSerie;
			cout << "ingrese el porcentaje de bateria: ";
			cin >> porcentajeBateria;
			cout << "ingrese el precio base: ";
			cin >> precioBase;

			cout << "Indique el tipo de Bicicleta" << endl;
			cout << "	0 - Paseo" << endl;
			cout << "	1 - Montaña" << endl;
			cin >> op_tipobici;
			if (op_tipobici==0){
				tipoBici= Paseo;
			}
			if (op_tipobici==1){
				tipoBici=Montania;
			}
			cout << "Ingrese la cantidad de cambios que tiene la bici: ";
			cin >> cantCambios;
			bicicleta = DtBicicleta (nroSerie, porcentajeBateria, precioBase, tipoBici, cantCambios);
			break;// fin de opcion 2
    } // fin del switch
}
//    int nroSerie = 0;
//    cout << "Ingrese nro de serie: ";
//    cin >> nroSerie;
//
//    if(existeNroSerie(nroSerie)){
//        cout << "El vehiculo ya existe";
//        cin.get();
//    }else{
//        float porcentajeBateria= 0.0, precioBase= 0.0;
//        string tipoVehiculo ="";
//        cin.ignore();
//        cout << "Ingrese el porcentaje de bateria: ";
//        cin >> porcentajeBateria;
//        if ((porcentajeBateria >= 1.0)&& (porcentajeBateria <= 100.0)){
//            cout << "Ingrese el precio base: ";
//            cin >> precioBase;
//            cout << "Ingrese " << '"' << "B" << '"' << " o " << '"' << "b" << '"' << " si es una Bicicleta o " << '"' << "M" << '"'
//                 << " o " << '"' << "m" << '"' << " si es un Monopatin: ";
//            cin >> tipoVehiculo;
//            if (tipoVehiculo == "B" || tipoVehiculo == "b") {
//                tipo tipoB = Paseo;
//                int opcionBici = 0;
//                cout << "Seleccione el tipo de bicicleta" << endl << endl;
//                cout << "    1-Paseo" << endl;
//                cout << "    2-Montania" << endl;
//                while (opcionBici > 2 || opcionBici <= 0) {
//                    cin >> opcionBici;
//                    if (opcionBici > 2 || opcionBici <= 0) {
//                        cout << "\n Ingrese una opción correcta: ";
//                    } else if (opcionBici == 1) {
//                        tipoB = Paseo;
//                    } else if (opcionBici == 2) {
//                        tipoB = Montania;
//                    }
//                }
//                int cantidad = 0;
//                cout << "Ingrese la cantidad de cambios de la Bicicleta: ";
//                cin >> cantidad;
//
//                // ingresar el DT bicicleta a la coleccion
//                DtBicicleta dtBicicleta = DtBicicleta (nroSerie, porcentajeBateria, precioBase, tipoB, cantidad);
//                //Viaje::agregarVehiculoV(dtBicicleta);
//
//            }else if (tipoVehiculo == "M" || tipoVehiculo == "m") {
//                bool luces = false;
//                int opcionLuces = 0;
//                cout << "Indique si el monopatin tiene luces" << endl << endl;
//                cout << "    1-SI" << endl;
//                cout << "    2-NO" << endl;
//                cout << " Ingrese una opción: ";
//                cin >> opcionLuces;
//                while (opcionLuces > 2 || opcionLuces <= 0) {
//                    cin >> opcionLuces;
//                    if (opcionLuces > 2 || opcionLuces <= 0) {
//                        cout << "\n Ingrese una opción correcta: ";
//                    } else if (opcionLuces == 1) {
//                        luces = true;
//                    } else if (opcionLuces == 2) {
//                        luces = false;
//                    }
//                }
//
//                // ingresar el DT Monopatin a la coleccion
//                DtMonopatin dtMonopatin = DtMonopatin (nroSerie, porcentajeBateria, precioBase, luces);
//                //Viaje::agregarVehiculoV(dtMonopatin);
//
//            }
//        }else{
//            cout << "Porcentaje de bateria no válido";
//            cin.get();
//        }
//    }


void opcion_3() {
    //Ingresar Viaje

    clearscr();
    cout << "Ingresar Viaje\n" << endl;
}

void opcion_4() {
    //Ver Viajes Antes de Fecha

    clearscr();
    cout << "Ver Viajes Antes de Fecha\n" << endl;
    if (colUsuarios.size()==0){
        cout << "No existen usuarios aun" << endl;
        cin.get();
        cin.get();
    }
    else{
        string ci = cedulaValida();
        if (existeUsuario(ci)){
            if (colUsuarios.find(ci)->second->cantViajes() > 0){
                int viajes;
                viajes=colUsuarios.find(ci)->second->cantViajes();
                cout << endl;
                cout << "el usuario de cédula " << ci << " tiene " << viajes << " de viajes" << endl;
                cout << endl;

                int dia;
                int mes;
                int anio;

                cout << "ingrese la fecha para ver los viajes anteriores" << endl;
                cout << "" << endl;
                cout << "ingrese el dia: ";
                cin >> dia;
                cout << "ingrese el mes: ";
                cin >> mes;
                cout << "ingrese el anio: ";
                cin >> anio;

                DtFecha fecha = DtFecha(dia,mes,anio);

                int viajesACopiar = colUsuarios.find(ci)->second->getViajesAntesFecha(fecha).size();
                cout << endl;
                cout << "La cantidad de viajes que cumplen con la condición son: " << viajesACopiar << endl;
                cout << endl;

                if (viajesACopiar != 0) {

                    map<string,DtViaje> aux = colUsuarios.find(ci)->second->getViajesAntesFecha(fecha);
                    cout << " " << left << setw(12) <<"CLAVE" << left << setw(35) << "MOTIVO"<< "FECHA DE CONSULTA \n" << endl;
                    for (map<string,DtViaje>::iterator it = aux.begin();
                         it != aux.end();
                         ++it) {
                        cout << " " << left<< setw(12) << it->first << left << setw(35) << it->second.getDistancia() << left <<it->second.getDuracion() << left <<it->second.getFecha().getDia() << ":" << it->second.getFecha().getMes() << ":" << it->second.getFecha().getAnio() << endl;
                    }

                }else{
                    cout << "No existen viajes antes de la fecha ingresada." << endl;
                }
            } // fin del existeSocio
            else{
                cout << "El usuario no tiene consultas." << endl;
                cin.get();
            }
        } // fin de existe socio
        else{
            cout << "No existe el usuario." << endl;
            cin.get();
        }

        cin.ignore();
        cin.get();
    }
}

void opcion_5() {
    //Eliminar Viajes

    clearscr();
    cout << "Eliminar Viajes\n" << endl;
}

void opcion_6() {
    //Listar Usuarios

    clearscr();
    cout << "Listar Usuarios\n" << endl;
    listarUsuarios();
}

void opcion_7() {
    //Listar Vehículos

    clearscr();
    cout << "Listar Vehículos\n" << endl;
    clearscr();

    string ci = cedulaValida();

    if (existeUsuario(ci)){
        //obtenerVehiculos(ci); //implementar dicha funcion
    } else {
        cout << "El Usuario " << ci << " no está registrado." << endl;
    }
    cin.ignore();
    cin.get();
}

void opcion_8() {
    //Cambiar Bateria Vehículo

    clearscr();
    cout << "Cambiar Bateria Vehículo\n" << endl;
}

int main(){

    string opt;

    do {
        menu();
        cin >> opt;

        try {
            if (opt == "1") {
                cout <<"Opcion 1" <<endl;
                opcion_1();
            }
            else if (opt == "2") {
                cout <<"Opcion 2" <<endl;
                opcion_2();
            }
            else if (opt == "3") {
                cout <<"Opcion 3" <<endl;
                opcion_3();
            }
            else if (opt == "4") {
                cout <<"Opcion 4" <<endl;
                opcion_4();
            }
            else if (opt == "5") {
                cout <<"Opcion 5" <<endl;
                opcion_5();
            }
            else if (opt == "6") {
                opcion_6();
            }
            else if (opt == "7") {
                cout <<"Opcion 7" <<endl;
                opcion_7();
            }
            else if (opt == "8") {
                cout <<"Opcion 8" <<endl;
                opcion_8();
            }
        }
        catch (invalid_argument &e) {
            cout << "Opcion no válida!"<< endl;
        }

    } while (opt != "0");
    return 0;
}
