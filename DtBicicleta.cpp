//
// Created by fabio on 17/03/20.
//

#include "DtVehiculo.h"
#include "DtBicicleta.h"

//Constructores
DtBicicleta::DtBicicleta() : DtVehiculo() {
    this->TipoBici = Paseo;
    this->cantCambios = 0;
}

DtBicicleta::DtBicicleta(int nroSerie, float porcentajeBateria, float precioBase, tipo TipoBici, int cantCambios): DtVehiculo(nroSerie, porcentajeBateria, precioBase) {
    this->TipoBici = TipoBici;
    this->cantCambios = cantCambios;
}

//Geters
tipo DtBicicleta::getTipoBici(){
    return this->TipoBici;
}

int DtBicicleta::getcantCambios(){
    return this->cantCambios;
}

//Destructor
DtBicicleta::~DtBicicleta() = default;