//
// Created by fabio on 17/03/20.
//

#include "Vehiculo.h"

//Constructores
Vehiculo::Vehiculo(){
    this->nroSerie = 0;
    this->porcentajeBateria = 100.0;
    this->precioBase = 0.0;
}

Vehiculo::Vehiculo(int nroSerie, float porcentajeBateria, float precioBase){
    this->nroSerie = nroSerie;
    this->porcentajeBateria = porcentajeBateria;
    this->precioBase = precioBase;
}

//Seters
void Vehiculo::setNroSerie(int nroSerie){
    this->nroSerie = nroSerie;
}

void Vehiculo::setPorcentajeBateria(float porcentajeBateria){
    this->porcentajeBateria = porcentajeBateria;
}

void Vehiculo::setPrecioBase(float precioBase){
    this->precioBase = precioBase;
}

//Geters
int Vehiculo::getNroSerie(){
    return this->nroSerie;
}

float Vehiculo::getPorcentajeBateria(){
    return this->porcentajeBateria;
}

float Vehiculo::getPrecioBase(){
    return this->precioBase;
}

//Destructor
Vehiculo::~Vehiculo() = default;