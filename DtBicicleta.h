//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_DTBICICLETA_H
#define PAV2020_LAB1_DTBICICLETA_H
#include "DtVehiculo.h"
#include "EnumTipoBici.h"

class DtBicicleta: public DtVehiculo {
private:
    tipo TipoBici;
    int cantCambios;

public:
    //Constructor
    DtBicicleta();
    DtBicicleta(int nroSerie, float porcentajeBateria, float precioBase, tipo TipoBici, int cantCambios);

    //Geters
    tipo getTipoBici();
    int getcantCambios();

    //Destructor
    ~DtBicicleta();
};

#endif //PAV2020_LAB1_DTBICICLETA_H
