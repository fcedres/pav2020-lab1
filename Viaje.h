//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_VIAJE_H
#define PAV2020_LAB1_VIAJE_H
#include "Vehiculo.h"
#include "Bicicleta.h"
#include "Monopatin.h"
#include "EnumTipoBici.h"
#include "DtFecha.h"
#include "DtVehiculo.h"
#include "DtBicicleta.h"
#include "DtMonopatin.h"
#include "Bicicleta.h"
#include "Monopatin.h"
#include <string>
#include <map>
#include "Constante.h"
#include "DtVehiculo.h"
#include "DtMonopatin.h"
#include "DtBicicleta.h"
using std::string;



class Viaje{

private:
    DtFecha fecha;
    int duracion;
    int distancia;
    Vehiculo* vehiculo;

public:

    //Constructores
    Viaje();
    Viaje(DtFecha fecha, int duracion, int distancia);

    //Seters
    void setFecha(DtFecha fecha);
    void setDuracion(int duracion);
    void setDistancia(int distancia);
    // agregado por edu
    void AgregarVehiculo(DtVehiculo vehiculo);

    //Geters
    DtFecha getFecha();
    int getDuracion();
    int getDistancia();


    //Destructor
    ~Viaje();
    DtVehiculo getVehiculo();
    int getCantVehiculos();
    void agregarVehiculoV(const DtVehiculo& dtVehiculo);
};
#endif //PAV2020_LAB1_VIAJE_H
