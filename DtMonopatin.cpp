//
// Created by fabio on 17/03/20.
//

#include "DtVehiculo.h"
#include "DtMonopatin.h"

//Constructores
DtMonopatin::DtMonopatin() : DtVehiculo() {
    this->tieneLuces = false;
}

DtMonopatin::DtMonopatin(int nroSerie, float porcentajeBateria, float precioBase, bool tieneLuces): DtVehiculo(nroSerie, porcentajeBateria, precioBase) {
    this->tieneLuces = tieneLuces;
}

//Geters
bool DtMonopatin::getTieneLuces(){
    return this->tieneLuces;
}

//Destructor
DtMonopatin::~DtMonopatin() = default;