//
// Created by fabio on 17/03/20.
//
#include "DtViajeBase.h"

//Constructores
DtViajeBase::DtViajeBase(){
    this->fecha = getFecha();
    this->duracion = 100;
    this->distancia = 0;
}

DtViajeBase::DtViajeBase(DtFecha fecha, int duracion, int distancia){
    this->fecha = fecha;
    this->duracion = duracion;
    this->distancia = distancia;
}

//Geters
DtFecha DtViajeBase::getFecha(){
    return this->fecha;
}

int DtViajeBase::getDuracion(){
    return this->duracion;
}

int DtViajeBase::getDistancia(){
    return this->distancia;
}

//Destructor
DtViajeBase::~DtViajeBase() = default;