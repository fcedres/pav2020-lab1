//
// Created by fabio on 17/03/20.
//

#include "DtVehiculo.h"

//Constructores
DtVehiculo::DtVehiculo(){
    this->nroSerie = 0;
    this->porcentajeBateria = 0.0;
    this->precioBase = 0.0;
}

DtVehiculo::DtVehiculo(int nroSerie, float porcentajeBateria, float precioBase){
    this->nroSerie = nroSerie;
    this->porcentajeBateria = porcentajeBateria;
    this->precioBase = precioBase;
}

//Getters
int DtVehiculo::getNroSerie(){
    return this->nroSerie;
}

float DtVehiculo::getPorcentajeBateria(){
    return this->porcentajeBateria;
}

float DtVehiculo::getPrecioBase(){
    return this->precioBase;
}

//Destructor
DtVehiculo::~DtVehiculo() = default;