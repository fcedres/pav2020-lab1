//
// Created by fabio on 18/03/20.
//

#ifndef PAV2020_LAB1_BICICLETA_H
#define PAV2020_LAB1_BICICLETA_H
#include "Vehiculo.h"
#include "EnumTipoBici.h"
#include <string>
using std::string;



class Bicicleta: public Vehiculo{

private:
    tipo TipoBici;
    int cantCambios;

public:

    //Constructores
    Bicicleta();
    Bicicleta(int nroSerie, float porcentajeBateria, float precioBase, tipo TipoBici, int cantCambios);

    //Seters
    void setTipo(tipo TipoBici);
    void setCantCambios(int cantCambios);

    //Geters
    tipo getTipoBici();
    int getCantCambios();

    //Destructor
    ~Bicicleta();

    float darPrecioViaje(int duracion, int distancia);

};
#endif //PAV2020_LAB1_BICICLETA_H
