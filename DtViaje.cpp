//
// Created by fabio on 17/03/20.
//

#include "DtViaje.h"

//Constructores
DtViaje::DtViaje() : DtViajeBase(){
    this->precioTotal = 0.0;
    this->vehiculo = DtVehiculo();
}

DtViaje::DtViaje(DtFecha fecha, int duracion, int distancia, float precioTotal, DtVehiculo vehiculo): DtViajeBase(fecha, duracion, distancia){
    this->precioTotal = precioTotal;
    this->vehiculo = vehiculo;
}

/*DtViaje::DtViaje(float precioTotal, DtVehiculo vehiculo){//: DtViajeBase(DtFecha fecha, int duracion, int distancia){
    this->precioTotal = precioTotal;
    this->vehiculo = vehiculo;
}*/

//Getters
float DtViaje::getPrecioTotal(){
    return precioTotal;
}

DtVehiculo DtViaje::getVehiculo() {
    return vehiculo;
}



//Destructor
DtViaje::~DtViaje() = default;
