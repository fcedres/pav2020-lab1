//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_DTVIAJEBASE_H
#define PAV2020_LAB1_DTVIAJEBASE_H
#include <string>
#include "DtFecha.h"

using std::string;


class DtViajeBase {
private:
    DtFecha fecha;
    int duracion;
    int distancia;

public:
    //Constructores
    DtViajeBase();
    DtViajeBase(DtFecha fecha, int duracion, int distancia);

    //Geters
    DtFecha getFecha();
    int getDuracion();
    int getDistancia();

    //Destructores
    ~DtViajeBase();
};
#endif //PAV2020_LAB1_DTVIAJEBASE_H
