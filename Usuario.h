//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_USUARIO_H
#define PAV2020_LAB1_USUARIO_H
#include <string>
#include <utility>
#include <map>
#include <iomanip>
#include <typeinfo>
#include "Viaje.h"
#include "DtViaje.h"
#include "DtViajeBase.h"
#include "DtFecha.h"
using namespace std;

class Usuario {

private:
    string ci;
    string nombre;
    DtFecha fechaIngreso;
    map<string, Viaje*> colViaje;

public:
    //Constructores
    Usuario();

    Usuario(string ci, string nombre, DtFecha fechaIngreso);

    //Seters
    void setCi(string ci);
    void setNombre(string nombre);
    void setFechaIngreso(DtFecha fechaIngreso);

    //Geters
    string getCi();
    string getNombre();
    DtFecha getFechaIngreso();
    //map<string, DtViajeBase*> getViajes();

    //Destructor
    ~Usuario();
    DtFecha ObtenerFechaSistema();
    void ingresarViaje(string ci, int nroSerieVehiculo, const DtViajeBase& viaje);
    //void ingresarViajeB (DtViaje viaje);
    DtViaje verViajesAntesDeFecha(const DtFecha& fecha, string ci, int& cantViajes);
    void eliminarViajes(string ci, const DtFecha& fecha);
    void getViajes();
    int getCantVehiculos();
    map<string,DtViaje> getViajesAntesFecha(DtFecha fechaIngresada);
    int cantViajes();
    long getCantViajes();
    //fin funciones auxiliares

};
//SOBRECARGA
bool operator < (const DtFecha fecha1, const DtFecha fecha2);
#endif //PAV2020_LAB1_USUARIO_H
