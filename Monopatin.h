//
// Created by fabio on 18/03/20.
//

#ifndef PAV2020_LAB1_MONOPATIN_H
#define PAV2020_LAB1_MONOPATIN_H
#include "Vehiculo.h"
#include <string>
using std::string;



class Monopatin: public Vehiculo{

private:
    bool tieneLuces;

public:

    //Constructores
    Monopatin();
    Monopatin(int nroSerie, float porcentajeBateria, float precioBase, bool tieneLuces);

    //Seters
    void setTieneLuces(bool tieneLuces);

    //Geters
    bool getTieneLuces();

    //Destructor
    ~Monopatin();

    float darPrecioViaje(int duracion, int distancia);

};
#endif //PAV2020_LAB1_MONOPATIN_H
