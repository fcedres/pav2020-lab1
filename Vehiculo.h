//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_VEHICULO_H
#define PAV2020_LAB1_VEHICULO_H

#include <string>
#include "DtMonopatin.h"
#include "DtBicicleta.h"
using std::string;

class Vehiculo {
private:
    int nroSerie;
    float porcentajeBateria;
    float precioBase;

public:
    //constructor
    Vehiculo();

    Vehiculo(int nroSerie, float porcentajeBateria, float precioBase);

    //Seters
    void setNroSerie(int nroSerie);
    void setPorcentajeBateria(float porcentajeBateria);
    void setPrecioBase(float precioBase);

    //Geters
    int getNroSerie();
    float getPorcentajeBateria();
    float getPrecioBase();

    //Destructor
    ~Vehiculo();

    // Una operacion 'virtual' es una operacion que no se implementa en la propia clase,
    // y se hereda a los hijos. La sintaxis es: 'virtual <tipoRetorno> <nombreFuncion>() = 0'.
    virtual float darPrecioViaje()= 0;


};
#endif //PAV2020_LAB1_VEHICULO_H
