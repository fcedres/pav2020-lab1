//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_DTFECHA_H
#define PAV2020_LAB1_DTFECHA_H
#include <ctime>
#include <iostream>
#include <sstream>
#include <cstdlib>

using namespace std;

class DtFecha {

private:
    int dia;
    int mes;
    int anio;

public:
    //constructor
    DtFecha();

    DtFecha(int dia, int mes, int anio);

    //Seters
    void setDia(int dia);

    void setMes(int mes);

    void setAnio(int anio);

    //Geters
    int getDia();

    int getMes();

    int getAnio();

    //Destrucot
    ~DtFecha();
};

#endif //PAV2020_LAB1_DTFECHA_H
