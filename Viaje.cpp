//
// Created by fabio on 17/03/20.
//

#include "Viaje.h"

//Constructores
Viaje::Viaje(){
//    this->fecha = "";
    DtFecha fecha;
	this->fecha = fecha;
    this->duracion = 100;
    this->distancia = 0;
}

Viaje::Viaje(DtFecha fecha, int duracion, int distancia){
    this->fecha = fecha;
    this->duracion = duracion;
    this->distancia = distancia;
}

//Seters
void Viaje::setFecha(DtFecha fecha){
    this->fecha = fecha;
}

void Viaje::setDuracion(int duracion){
    this->duracion = duracion;
}

void Viaje::setDistancia(int distancia){
    this->distancia = distancia;
}

// agregado por edu
void Viaje::AgregarVehiculo(DtVehiculo vehiculo){
//	this->vehiculo = nuevo;
}

//Geters
DtFecha Viaje::getFecha(){
    return this->fecha;
}

int Viaje::getDuracion(){
    return this->duracion;
}

int Viaje::getDistancia(){
    return this->distancia;
}
int Viaje::getCantVehiculos(){
   // return colVehiculos.size(); // el viaje tiene un solo vehiculo asociado
	return 0;
}

//map<int, DtVehiculo*> Viaje::getVehiculos(){
//
//    map<int, DtVehiculo*> vehiculos;
//    for(map<int, Vehiculo*>::iterator it=colVehiculos.begin(); it!=colVehiculos.end(); ++it) {
//
//
//        try {
//
//            Bicicleta* bicicleta = dynamic_cast<Bicicleta*> (it->second);
//
//            if (bicicleta != nullptr) {
//                DtBicicleta* dtbicicleta = new DtBicicleta(bicicleta->getNroSerie(), bicicleta->getPorcentajeBateria(), bicicleta->getPrecioBase(),
//                                               bicicleta->getTipoBici(), bicicleta->getCantCambios());
//
//                vehiculos.insert(make_pair(it->first, dtbicicleta));
//            }
//
//        } catch (std::bad_cast &e) { cout << "error casteo." << endl; }
//
//        try {
//            Monopatin* monopatin = dynamic_cast<Monopatin*> (it->second);
//
//            if (monopatin != nullptr) {
//                DtMonopatin* dtmonopatin = new DtMonopatin(monopatin->getNroSerie(), monopatin->getPorcentajeBateria(), monopatin->getPrecioBase(),
//                                            monopatin->getTieneLuces());
//
//                vehiculos.insert(make_pair(it->first, dtmonopatin));
//            }
//        } catch (std::bad_cast &e) { cout << "error casteo." << endl; }
//
//    }
//    return vehiculos;
//}

void Viaje::agregarVehiculoV(const DtVehiculo& dtVehiculo) {
/*

        try{ // CASTEO DINAMICO DE CLASES DE MASCOTA A DTPERRO O DTGATO

            auto dtbicicleta = dynamic_cast<const DtBicicleta&>(dtVehiculo);
            Bicicleta* bicicleta = new Bicicleta(dtbicicleta.getNroSerie(), dtbicicleta.getPorcentajeBateria(), dtbicicleta.getPrecioBase(), dtbicicleta.getTipoBici(), dtbicicleta.getcantCambios());
            this->colVehiculos.insert(make_pair(dtbicicleta.getNroSerie(),bicicleta));

        }catch (std::bad_cast &e) {}

        try{
            auto dtmonopatin = dynamic_cast<const DtMonopatin&> (dtVehiculo);
            Monopatin* monopatin = new Monopatin(dtmonopatin.getNroSerie(), dtmonopatin.getPorcentajeBateria(), dtmonopatin.getPrecioBase(), dtmonopatin.getTieneLuces());
            this->colVehiculos.insert(make_pair(dtmonopatin.getNroSerie(),monopatin));

        }catch (std::bad_cast &e) {}
*/
}


//agregado por edu
DtVehiculo Viaje::getVehiculo(){

	int nroSerie;
	float porcentajeBateria,precioBase,precioViaje;

	nroSerie = this->vehiculo->getNroSerie();
	porcentajeBateria = this->vehiculo->getPorcentajeBateria();
	precioBase = this->vehiculo->getPrecioBase();
	precioViaje = 0; //falta arreglar esto

	DtVehiculo aux = DtVehiculo(nroSerie,porcentajeBateria,precioBase);

	return aux;
}
//hasta aca

//Destructor
Viaje::~Viaje() = default;
