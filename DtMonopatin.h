//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_DTMONOPATIN_H
#define PAV2020_LAB1_DTMONOPATIN_H
#include "DtVehiculo.h"


class DtMonopatin: public DtVehiculo {
private:
    bool tieneLuces;

public:
    //Constructor
    DtMonopatin();
    DtMonopatin(int nroSerie, float porcentajeBateria, float precioBase, bool tieneLuces);

    //Geters
    bool getTieneLuces();

    //Destructor
    ~DtMonopatin();
};
#endif //PAV2020_LAB1_DTMONOPATIN_H
