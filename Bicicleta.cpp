//
// Created by fabio on 18/03/20.
//

#include "Bicicleta.h"

//Constructores

Bicicleta::Bicicleta(){
    this->TipoBici = Paseo;
    this->cantCambios = 0;
}


Bicicleta::Bicicleta(int nroSerie,float porcentajeBateria, float precioBase, tipo Tipobici, int cantcambios): Vehiculo(nroSerie, porcentajeBateria, precioBase){
    this->TipoBici = Tipobici;
    this->cantCambios = cantcambios;
}

/*Bicicleta::Bicicleta(int nroSerie,float porcentajeBateria,float PrecioBase,enum tipo Tipobici,int cantcambios){//(tipo Tipobici, int cantcambios){
//: Vehiculo(int nroSerie, float porcentajeBateria, float precioBase){
    this->TipoBici = Tipobici;
    this->cantCambios = cantcambios;
}*/


//Seters
void Bicicleta::setTipo(tipo TipoBici){
	this->TipoBici = TipoBici;
}

void Bicicleta::setCantCambios(int cantCambios) {
    this->cantCambios = cantCambios;
}

//Geters
tipo Bicicleta::getTipoBici() {
    return this->TipoBici;
}

int Bicicleta::getCantCambios(){
    return this->cantCambios;
}

//Destructor
Bicicleta::~Bicicleta() = default;

//Other
float Bicicleta::darPrecioViaje(int duracion, int distancia){
    float precio = getPrecioBase();
    //int distancia = distancia();
    return ((precio*distancia));
}
