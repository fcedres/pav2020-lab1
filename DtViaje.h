//
// Created by fabio on 17/03/20.
//

#ifndef PAV2020_LAB1_DTVIAJE_H
#define PAV2020_LAB1_DTVIAJE_H
#include <string>
#include "DtViajeBase.h"
#include "DtVehiculo.h"


using std::string;


class DtViaje : public DtViajeBase{
private:
    float precioTotal;
    DtVehiculo vehiculo;

public:
    //Constructores
    DtViaje();
    DtViaje(DtFecha fecha, int duracion, int distancia, float precioTotal, DtVehiculo vehiculo);

    //Geters
    float getPrecioTotal();
    DtVehiculo getVehiculo();
    //Destructores
    ~DtViaje();
};

#endif //PAV2020_LAB1_DTVIAJE_H
