//
// Created by fabio on 17/03/20.
//

#include "DtFecha.h"

//Constructores
DtFecha::DtFecha() {
    time_t now = time(0);
    tm *ltm = localtime(&now);
    this->dia = ltm->tm_mday;
    this->mes = 1 + ltm->tm_mon ;
    this->anio = 1900 + ltm->tm_year;
}

DtFecha::DtFecha(int dia, int mes, int anio) {
    this->dia = dia;
    this->mes = mes;
    this->anio = anio;
}

//Geters
int DtFecha::getDia() {
    return this->dia;
}

int DtFecha::getMes() {
    return this->mes;
}

int DtFecha::getAnio() {
    return this->anio;
}

//Destructor
DtFecha::~DtFecha() = default;